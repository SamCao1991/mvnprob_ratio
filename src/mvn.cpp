#include <RcppEigen.h>
#include <R_ext/Print.h>
#include <R.h>
#include <algorithm>
#include <chrono>
#include <ctime>
#include "mvnkernel.h"
#include "misc.h"

using namespace std;
using namespace Eigen;

typedef std::chrono::time_point<std::chrono::steady_clock> TimeStamp;


double mvn_ratio(int N, const MatrixXd& L, const VectorXd &a1, const VectorXd &b1,
        int i0, int ns, int &scaler_in, double *workDbl, int lworkDbl,
        int *workInt, int lworkInt)
{
        int n = L.rows();
	if(i0 < 0 || i0 >= n)
		return 1.0;
	double ratio;
        // dbl work
        double *pN = workDbl; // 2*N
        double *y = pN + 2*N; // 2*n*N
        double *values = y + 2*n*N; // ns
        Map<MatrixXd> a(values + ns, n, N << 1); // 2*n*N
        Map<MatrixXd> b(a.data()+2*n*N, n, N << 1); // 2*n*N
        Map<MatrixXd> qN(b.data()+2*n*N, n, N); // (n)*N
        Map<MatrixXd> x(qN.data()+(n)*N, n, N << 1); // 2*n*N
        Map<VectorXd> xr(x.data()+2*n*N, n); // n
	double *p0 = xr.data() + n; // 2*N
	double *p0Values = p0 + 2*N; // ns
        double *subworkDbl = p0Values + ns; // 6*(2*N)
        int lsubworkDbl = 12*N;
        int memDbl = 2*N + 2*n*N + ns + 2*n*N + 2*n*N + (n)*N + 2*n*N + n + 2*N + ns +
                12*N; // 9*n*N + n + 2*ns + 16*N
        if(lworkDbl < memDbl)
                Rcpp::stop("Insufficient memory for mvt \n");
        // int work
        int *prime = workInt;
        int *scaler_N = prime + n;
        int *scaler_ns = scaler_N + 2*N;
        int *subworkInt = scaler_ns + ns;
        int lsubworkInt = 2*N;
        int memInt = n + 2*N + ns + 2*N;
        if(lworkInt < memInt)
                Rcpp::stop("Insufficient memory for mvt \n");
        // generate n prime numbers
        primes(5*(n+1)*log((double)(n+1)+1)/4, n, prime);
        // qN = prime * one2N
        transform(prime, prime+n, qN.data(), [](int x){return
                sqrt((double) x);});
        for(int i = 1; i < qN.cols(); i++)
                transform(qN.data(), qN.data()+n, qN.col(i-1).data(), qN.col(i).
                        data(), [](double x1,double x2){return x1 + x2;});
        // a, b
	for(int i = 0; i < (N<<1); i++)
	{
		copy_n(a1.data(), n, a.col(i).data());
		copy_n(b1.data(), n, b.col(i).data());
	}
        // MC
        for(int i = 0; i < ns; i++)
        {
                fill(scaler_N, scaler_N+2*N, 0);
                // x from R RNG
		GetRNGstate();
                for_each(xr.data(), xr.data()+n, [](double &x){x = unif_rand();});
		PutRNGstate();
                for(int j = 0; j < N; j++)
                        transform(qN.col(j).data(), qN.col(j).data()+n, xr.data(),
                                x.col(j).data(), [](double &xFix, double &xr){
                                return xFix + xr;});
                for_each(x.data(), x.data()+n*N, [](double &x){x = abs(2.0 *
                        (x - int(x)) - 1.0);});
                transform(x.data(), x.data()+n*N, x.data()+n*N, [](double &x){return
                        1 - x;});

                // kernel
                mvndns(n, N<<1, L, x, a, b, i0, pN, p0, y, n, scaler_N, 
			subworkDbl, lsubworkDbl, subworkInt, lsubworkInt);
                // scale
                int scaler_max = *(std::max_element(scaler_N, scaler_N+2*N));
                std::transform(scaler_N, scaler_N+2*N, pN, pN,
                        [scaler_max](int scaler, double base){return scalbn(base,
                        scaler - scaler_max);});
                // store
                values[i] = accumulate(pN, pN+(N<<1), 0.0);
		transform(p0, p0 + 2*N, pN, p0, [](double x, double y){return x*y;});
		p0Values[i] = accumulate(p0, p0 + 2*N, 0.0);
                scaler_ns[i] = scaler_max;
        }
        int scaler_max = *(std::max_element(scaler_ns, scaler_ns+ns));
        std::transform(scaler_ns, scaler_ns+ns, values, values,
                [scaler_max](int scaler, double base){return scalbn(base,
                scaler - scaler_max);});
        std::transform(scaler_ns, scaler_ns+ns, p0Values, p0Values,
                [scaler_max](int scaler, double base){return scalbn(base,
                scaler - scaler_max);});
	ratio = accumulate(p0Values, p0Values + ns, 0.0) / accumulate(values,
		values + ns, 0.0);
        return ratio;
}


