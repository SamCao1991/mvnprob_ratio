#ifndef MVNKERNEL_H
#define MVNKERNEL_H
#include <RcppEigen.h>
#include <vector>
int mvndns(int n,  int N, const Eigen::MatrixXd& L, const Eigen::MatrixXd &x,
       const Eigen::MatrixXd &a, const Eigen::MatrixXd &b, int i0,
       double *p, double *p0, double *y, int ldy, int *scaler,
       double *workDbl, int lworkDbl, int *workInt, int lworkInt);
#endif
