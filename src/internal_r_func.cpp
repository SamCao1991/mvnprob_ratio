// -*- mode: C++; c-indent-level: 4; c-basic-offset: 4; indent-tabs-mode: nil; -*-

// we only include RcppEigen.h which pulls Rcpp.h in for us
#include <RcppEigen.h>
#include <R_ext/Lapack.h>
#include <R_ext/Print.h>
#include <numeric>
#include <cmath>
#include <algorithm>
#include <functional>
#include <chrono>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "mvphi.h"
#include "morton.h"
#include "mvnkernel.h"
#include "misc.h"
#include "cholesky.h"
#include "mvn.h"
#include "uni_reorder.h"

using namespace std;
using namespace Eigen;

typedef std::chrono::time_point<std::chrono::steady_clock> TimeStamp;

// [[Rcpp::export]]
Rcpp::List mvn_ratio(Eigen::VectorXd a, Eigen::VectorXd b, Eigen::MatrixXd 
	covM, int N)
{
	// tmp var
	int n = covM.rows();
	int ns = 10;
	int fail;
	double v;
	int scaler;
	TimeStamp start, end;
	double timeChol, timeInt;
	// scale a, b, covM
	{
		VectorXd diagVec = covM.diagonal();
		diagVec.noalias() = diagVec.unaryExpr([](double x){return 
			1.0/sqrt(x);});
		auto diagM = diagVec.asDiagonal();
		a.noalias() = diagM * a;
		b.noalias() = diagM * b;
		covM.noalias() = diagM * covM;
		covM.noalias() = covM * diagM;
	}
	// mem alloc
	int lworkDbl = 9*n*N + n + 2*ns + 16*N;
	int lworkInt = max(4*N + n + ns + 1, 2*n);
	double *workDbl;
	int *workInt;
	workDbl = new double[lworkDbl];
	workInt = new int[lworkInt];
	// uni reorder
	start = std::chrono::steady_clock::now();
	double *y = workDbl;
	double *acp = y + n;
	double *bcp = acp + n;
	double *subworkDbl = bcp + n;
	int lsubworkDbl = 6*n;
	int *idx = workInt;
	int *subworkInt = idx + n;
	int lsubworkInt = n;
	copy(a.data(), a.data()+n, acp);
	copy(b.data(), b.data()+n, bcp);
	iota(idx, idx+n, 0);
	fail = uni_reorder(n, covM.data(), covM.rows(), acp, bcp, v, y, idx, 
		subworkDbl, lsubworkDbl);
	if(fail)
		Rcpp::stop("Cholesky failed. Please check the positive definiteness "
			"of the input covariance matrix\n");
	reorder(a.data(), idx, n, subworkInt, lsubworkInt);
	reorder(b.data(), idx, n, subworkInt, lsubworkInt);
	if(idx[n-1] != n-1)
		Rcpp::stop("The last variable should not be reordered.\n");
	int i0 = n - 1;
	end = std::chrono::steady_clock::now();
	timeChol = std::chrono::duration<double>(end - start).count();
	// call the mvn function
	start = std::chrono::steady_clock::now();
	double ratio = mvn_ratio(N, covM, a, b, i0, ns, scaler, workDbl, lworkDbl, 
		workInt, lworkInt);
	end = std::chrono::steady_clock::now();
	timeInt = std::chrono::duration<double>(end - start).count();
	// mem release
	delete[] workDbl;
	delete[] workInt;
	// return a list
	return Rcpp::List::create(Rcpp::Named("Ratio") = ratio, 
		Rcpp::Named("Univariate reordering time") = timeChol,
		Rcpp::Named("Monte Carlo time") = timeInt);
}


